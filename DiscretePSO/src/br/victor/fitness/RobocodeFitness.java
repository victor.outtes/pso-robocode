package br.victor.fitness;

import java.io.File;
import java.util.Arrays;

import br.victor.util.Constants;
import robocode.BattleResults;
import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;
import robocode.control.events.BattleAdaptor;
import robocode.control.events.BattleCompletedEvent;

public class RobocodeFitness {
	
	double fitness = 0.0;
	
	public double calculate(boolean last) {
		BattlefieldSpecification battlefield;
		RobocodeEngine engine = new RobocodeEngine(new File(Constants.ROBOCODE));
		engine.setVisible(false);
		engine.addBattleListener(new BattleAdaptor() {
			double sum, score;

			@Override
			public void onBattleCompleted(BattleCompletedEvent event) {
				BattleResults[] list = event.getIndexedResults();
				assert list.length == 2;
				sum = Arrays.stream(list).mapToInt(a -> a.getScore()).sum();
				for (BattleResults br : list) {
					if (last) {
						System.out.println(br.getRank() + " - " + br.getTeamLeaderName() + " (" + br.getScore() + ")");
					}
					if (br.getTeamLeaderName().startsWith(Constants.ROBOT)) {
						score = br.getScore();
					}
				}
				fitness = score / sum;
			}
		});

		battlefield = new BattlefieldSpecification(800, 600);
		final RobotSpecification[] selectedRobots = engine
				.getLocalRepository(String.join(",", Constants.RIVAL, Constants.ROBOT));
		final BattleSpecification battleSpec = new BattleSpecification(battlefield, 3, 450, 0.1, 100, false, selectedRobots);
		
		engine.runBattle(battleSpec, true);
		engine.close();
		return fitness;
	}

}
