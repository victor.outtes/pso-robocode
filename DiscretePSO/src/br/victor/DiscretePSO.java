package br.victor;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import br.victor.fitness.RobocodeFitness;
import br.victor.particle.DiscreteGenes;
import br.victor.particle.DiscreteParticle;

public class DiscretePSO {
	
	public static void main(String[] args) {
		new DiscretePSO().run();
	}

	public void run() {
		//Definitions
		int particleSize = 12;
		int numParticles = 10;
		int maxIterations = 50;
		DiscreteParticle globalBest = null; 
		
		
		//Particles initialization
		DiscreteParticle[] population = new DiscreteParticle[numParticles];
		for (int i = 0; i < population.length; i++) {
			population[i] = new DiscreteParticle(particleSize);
		}
		
		
		//Do for max iterations...
		int actualIteration = 1;
		while (actualIteration < maxIterations) {
			//Particles fitness...
			for (int i = 0; i < population.length; i++) {
				DiscreteParticle particle = population[i].copy();
				particle.toJavaFile();
				RobocodeFitness robocode = new RobocodeFitness();
				try {
					particle.setValue(robocode.calculate(false));
				} catch (Exception e) {
					particle.setValue(0.0);
				}
				
				//LOCAL update
				if (particle.getBest() == null || particle.getBest().getValue() < particle.getValue()) {
					particle.setBest(particle.copy());
				}
				
				//GLOBAL update
				if (globalBest == null || globalBest.getValue() < particle.getValue()) {
					globalBest = particle.copy();
				}
				
				population[i] = particle;
			}
			
			//Update particles...
			for (int i = 0; i < population.length; i++) {
				DiscreteParticle particle = population[i].copy();
				particle = updateParticle(particle, globalBest.copy());
				population[i] = particle;
			}
			
			
			System.out.println("\n");
			System.out.println("----------------------------------------");
			System.out.println(actualIteration + "/" + maxIterations + ": Partial best particle:");
			System.out.println(globalBest);
			actualIteration++;
		}
		
		System.out.println("\n");
		System.out.println("----------------------------------------");
		System.out.println("Best particle:");
		System.out.println(globalBest);
		
		globalBest.toJavaFile();
		new RobocodeFitness().calculate(true);
	}
	
	private DiscreteParticle updateParticle(DiscreteParticle particle, DiscreteParticle best) {
		//Cognitive component (c1) and Social component (c2)
		double c1 = 0.5, c2 = 0.5;
		double[] c1r1 = IntStream.range(0, particle.getData().length).mapToDouble(i -> ThreadLocalRandom.current().nextDouble(0, c1)).toArray();
		double[] cognitive = IntStream.range(0, c1r1.length).mapToDouble(i -> c1r1[i] * (particle.getBest().getData()[i] - particle.getData()[i])).toArray();
		
		double[] c2r2 = IntStream.range(0, particle.getData().length).mapToDouble(i -> ThreadLocalRandom.current().nextDouble(0, c2)).toArray();
		double[] social = IntStream.range(0, c2r2.length).mapToDouble(i -> c2r2[i] * (best.getData()[i] - particle.getData()[i])).toArray();
		
		//speed update
		particle.setSpeed(IntStream.range(0, cognitive.length).mapToDouble(i -> cognitive[i] + social[i] + particle.getSpeed()[i]).toArray());
		
		for (int i = 0; i < particle.getData().length; i++) {
			int newValue = (int)Math.round(particle.getData()[i] + particle.getSpeed()[i]);
			if (newValue < 0) {
				newValue = 0;
			}
			if (newValue > DiscreteGenes.genes.length - 1) {
				newValue = DiscreteGenes.genes.length - 1;
			}
			particle.setData(i, newValue);
		}
		
		return particle;
	}
	
}
