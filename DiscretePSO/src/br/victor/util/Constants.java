package br.victor.util;

public class Constants {

	public static final String ROBOCODE = "C:/robocode";
	public static final String ROBOT = "victor.TempRobot";
	public static final String RIVAL = "sample.Walls";
	public static final String PATH_SAVE_ROBOT = ROBOCODE + "/robots/victor/TempRobot.java";
	public static final String COMPILE_CLASSPARH = ROBOCODE + "/libs/robocode.jar";
}
