package br.victor.particle;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import br.victor.util.Constants;

public class DiscreteParticle implements Serializable {

	private static final long serialVersionUID = 4783024789487729561L;
	int data[];
	double speed[];
	double value = 0;
	DiscreteParticle best;
	
	public DiscreteParticle() {
		
	}
	
	public DiscreteParticle(int size) {
		//initialize particle
		this.data = new int[size];
		this.speed = new double[size];
		for (int i = 0; i < data.length; i++) {
			this.data[i] = ThreadLocalRandom.current().nextInt(0, DiscreteGenes.genes.length - 1);
			this.speed[i] = ThreadLocalRandom.current().nextDouble(0, 1);
		}
	}
	
	public void toJavaFile() {
		String code = getCode();
		try {
			FileWriter writer = new FileWriter(Constants.PATH_SAVE_ROBOT);
			BufferedWriter out = new BufferedWriter(writer);
			out.write(code);
			out.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		try {
			Runtime.getRuntime().exec("javac -cp " + Constants.COMPILE_CLASSPARH + " " + Constants.PATH_SAVE_ROBOT);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public String getCode() {
		Set<String> runSet = new HashSet<>();
		for (int i = 0; i < 3; i++) {
			runSet.add(DiscreteGenes.genes[this.data[i]]);
		}
		
		Set<String> scannedRobotSet = new HashSet<>();
		for (int i = 3; i < 6; i++) {
			scannedRobotSet.add(DiscreteGenes.genes[this.data[i]]);
		}
		
		Set<String> onHitByBulletSet = new HashSet<>();
		for (int i = 6; i < 9; i++) {
			onHitByBulletSet.add(DiscreteGenes.genes[this.data[i]]);
		}
		
		Set<String> onHitWallSet = new HashSet<>();
		for (int i = 9; i < 12; i++) {
			onHitWallSet.add(DiscreteGenes.genes[this.data[i]]);
		}
		
		
		String run = String.join(";", runSet) + ";";
		String scannedRobot = String.join(";", scannedRobotSet) + ";";
		String onHitByBullet = String.join(";", onHitByBulletSet) + ";";
		String onHitWall = String.join(";", onHitWallSet) + ";";
		
		String code = 
				"package victor;" +
				"import robocode.*;" +
				"import static robocode.util.Utils.normalRelativeAngleDegrees;" +
				"public class TempRobot extends AdvancedRobot {" +
				"private byte moveDirection = 1;" +
				"private double bearingFromGun = 1;" +
				"private double distance = 0;" +
				"private double bearing = 0;" +
				"public void run() {" +
				"while(true) {" +
				"if (getVelocity() == 0) {moveDirection *= -1;}" +
				"%s" +
				"}" +
				"}" +
				"public void onScannedRobot(ScannedRobotEvent e) {" +
				"double distance = e.getDistance();" +
				"double absoluteBearing = getHeading() + e.getBearing();" +
				"double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading());" +
				"%s" +
				"}" +
				"public void onHitByBullet(HitByBulletEvent e) {" +
				"double distance = e.getDistance();" +
				"%s" +
				"}" +
				"public void onHitWall(HitWallEvent e) {" +
				"double distance = e.getDistance();" +
				"double bearing = e.getBearing();" +
				"%s" +
				"}" +
				"}";
		return String.format(code, run, scannedRobot, onHitByBullet, onHitWall);
	}
	
	public int[] getData() {
		return data;
	}
	
	public void setData(int index, int value) {
		this.data[index] = value;
	}
	
	public void setDataComplete(int[] value) {
		this.data = value;
	}
	
	public double[] getSpeed() {
		return speed;
	}
	
	public void setSpeed(double[] speed) {
		this.speed = speed;
	}
	
	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public DiscreteParticle getBest() {
		return best;
	}
	
	public void setBest(DiscreteParticle best) {
		this.best = best;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Data:").append("\n");
		for (int d : this.data) {
			sb.append(d + " ");
		}
		sb.append("\n");
		sb.append("Fitness: ").append(this.value);
		return sb.toString();
	}
	
	public DiscreteParticle copy() {
		DiscreteParticle dp = new DiscreteParticle();
		dp.setDataComplete(this.getData().clone());
		dp.setSpeed(this.getSpeed().clone());
		dp.setValue(this.getValue());
		
		if (this.best != null) {
			DiscreteParticle best = new DiscreteParticle();
			best.setDataComplete(this.getBest().getData());
			best.setValue(this.getBest().getValue());
			dp.setBest(best);
		}
		
		return dp;
	}
}